# Octoprint Discord Bot

- [Octoprint Discord Bot](#octoprint-discord-bot)
	- [About](#about)
	- [Configuring](#configuring)
	- [Usage](#usage)
	- [Development](#development)
	- [Publishing](#publishing)

## About

TBD

## Configuring

To use this bot the *Discord OAuth2 bot token* and *Octoprint application key* are needed alongside the Octoprint uri.

To configure, use the `appsettings.json` file located on the output of `dotnet publish`.

The file looks like this:

```json
{
	"DiscordToken": "BOT_TOKEN",
	"OctoprintApplicationKey": "OCTOPRINT_APPLICATION_KEY",
	"OctoprintUrl": "OCTOPRINT_URL",

	// ...
}

```

where `BOT_TOKEN`, `OCTOPRINT_APPLICATION_KEY` and `OCTOPRINT_URL` need to be replaced with the actual values.

Note that the config file needs to be in the ***same directory*** as the executable.

## Usage

TBD

## Development

Make sure you have the .NET 6 runtime installed.

It is recommended to run `dotnet restore` after cloning the project.

Create the `appsettings.Development.json` (it is ignored by git) file next to the `appsettings.json` and set the required parameters.

## Publishing

To publish the application run `dotnet publish -r <runtime>`.

Where `<runtime>` is to be replaced with the target OS and architecture - [see RID catalog](https://learn.microsoft.com/en-us/dotnet/core/rid-catalog).

You can use an optional `--self-contained` flag to publish the app as self-contained instead of being framework-dependent.

For example: `-r linux-x64 --self-contained`.

You can also enable single-file using `-p:PublishSingleFile=true`, but that is not necessary.

*Note that you might need to add `--ignore-failed-sources` flag if you have multiple NuGet package sources set.*