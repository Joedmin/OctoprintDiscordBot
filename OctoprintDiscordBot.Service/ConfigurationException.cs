namespace OctoprintDiscordBot.Service;

/// <summary>
/// Exception that is thrown when <see cref="IConfiguration"/> is missing required property.
/// </summary>
[System.Diagnostics.CodeAnalysis.SuppressMessage("Roslynator", "RCS1194:Implement exception constructors.", Justification = "There's no need.")]
public class ConfigurationException : Exception
{
	public ConfigurationException(string? message) : base(message)
	{
	}
}
