using OctoprintDiscordBot.Core;

namespace OctoprintDiscordBot.Service;

internal class Worker : BackgroundService
{
	private readonly InteractionHandler discordInteractionHandler;
	private readonly OctoprintClient octoprintClient;
	private readonly ILogger<Worker> logger;
	private bool disposed;

	public Worker(OctoprintClient octoprintClient, InteractionHandler discordInteractionHandler, ILogger<Worker> logger)
	{
		this.octoprintClient = octoprintClient;
		this.logger = logger;
		this.discordInteractionHandler = discordInteractionHandler;
	}

	protected override async Task ExecuteAsync(CancellationToken stoppingToken)
	{
		// TODO: Validate discord token the same way as the octoprint one
		// TODO: Stop service when either token is invalid
		// (https://learn.microsoft.com/en-us/dotnet/core/extensions/workers#signal-completion)
		if (!await octoprintClient.ConnectAsync(stoppingToken).ConfigureAwait(false))
		{
			logger.LogError("OctoPrint application key is not valid.");
		}

		await discordInteractionHandler.InitializeAsync().ConfigureAwait(false);

		while (!stoppingToken.IsCancellationRequested)
		{
			await Task.Delay(50, stoppingToken).ConfigureAwait(false);
		}
	}

	#region Dispose

	[System.Diagnostics.CodeAnalysis.SuppressMessage("Blocker Code Smell", "S2953:Methods named \"Dispose\" should implement \"IDisposable.Dispose\"",
		Justification = "Implemented by the ancestor.")]
	protected virtual void Dispose(bool disposing)
	{
		if (disposing && !disposed)
		{
			discordInteractionHandler?.Dispose();
			octoprintClient?.Dispose();
			disposed = true;
		}
	}

	~Worker() => Dispose(false);

	public override void Dispose()
	{
		Dispose(true);
		GC.SuppressFinalize(this);
		base.Dispose();
	}

	#endregion
}
