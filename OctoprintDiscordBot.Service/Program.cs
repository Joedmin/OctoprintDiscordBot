using Discord.Interactions;
using OctoprintDiscordBot.Core;
using OctoprintDiscordBot.Service;
using System.Net.Http.Headers;

// TODO: Document code
var host = Host.CreateDefaultBuilder(args)
	.UseSystemd()
	.ConfigureServices((context, services) =>
	{
		var discordToken = context.Configuration.GetValue<string>("DiscordToken");
		var octoprintKey = context.Configuration.GetValue<string>("OctoprintApplicationKey");
		var octoprintUriString = context.Configuration.GetValue<string>("OctoprintUrl");

		if (discordToken is null || octoprintKey is null || octoprintUriString is null)
		{
			throw new ConfigurationException("Partly or completely missing configuration in 'appsettings.json'.");
		}

		if (!Uri.TryCreate(octoprintUriString, UriKind.Absolute, out var octoprintUri))
		{
			throw new ConfigurationException("Invalid OctoPrint uri set in 'appsettings.json'.");
		}

		// TODO: https://stackoverflow.com/questions/37721959/trusting-self-signed-certificate-using-httpclient
		// TODO: https://stackoverflow.com/questions/70924783/httpclient-inject-into-singleton
		services.AddHttpClient<OctoprintClient>(configure =>
		{
			// For some reson `Bearer` token returns 403 even though it works in Postman
			// but that's not really an issue since custom auth header can be used:
			// https://docs.octoprint.org/en/master/api/general.html#authorization
			configure.DefaultRequestHeaders.Add("X-Api-Key", octoprintKey);
			configure.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
			configure.Timeout = TimeSpan.FromSeconds(2);
		});

		// TODO?: add options -> register services the old style.
		// This should also allow to update tokens without restart - do we want that?
		services.AddSingleton(x =>
			new OctoprintClient(octoprintUri,
				x.GetRequiredService<IHttpClientFactory>().CreateClient(nameof(OctoprintClient)),
				x.GetRequiredService<ILogger<OctoprintClient>>()));
		services.AddSingleton(x =>
			new DiscordClient(discordToken, x.GetRequiredService<ILogger<DiscordClient>>()));

		services.AddSingleton(x => new InteractionService(x.GetRequiredService<DiscordClient>().Rest, new InteractionServiceConfig()
		{
			// Command execution result is not awailable in `Async`
			// https://discordnet.dev/guides/int_framework/post-execution.html
			DefaultRunMode = RunMode.Sync,
		}));
		services.AddSingleton<InteractionHandler>();

		services.AddHostedService<Worker>();
	})
	.ConfigureLogging(configureLogging => configureLogging.AddConsole())
	.Build();

await host.RunAsync();
