using System.Text.Json.Serialization;

namespace OctoprintDiscordBot.Core.ResponseModels;

public record ServerVersionInfo()
{
	[JsonPropertyName("api")]
	public string ApiVersion { get; set; }

	[JsonPropertyName("server")]
	public string ServerVersion { get; set; }

	[JsonPropertyName("text")]
	public string OctoprintVersion { get; set; }
}
