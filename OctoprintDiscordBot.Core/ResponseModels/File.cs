using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace OctoprintDiscordBot.Core.ResponseModels;

public class File
{
	/// <summary>
	/// Upload date in unix timestamp.
	/// </summary>
	[JsonPropertyName("date")]
	[NotNull]
	public long? UploadDate { get; set; }

	[JsonPropertyName("name")]
	[NotNull]
	public string? Name { get; set; }

	/// <summary>
	/// Size in bytes.
	/// </summary>
	[JsonPropertyName("size")]
	[NotNull]
	public long? Size { get; set; }
}
