﻿using Newtonsoft.Json;

namespace OctoprintDiscordBot.Core.ResponseModels;

public class Bed
{
	[JsonProperty("actual")]
	public double Actual { get; set; }

	[JsonProperty("target")]
	public double Target { get; set; }
}
