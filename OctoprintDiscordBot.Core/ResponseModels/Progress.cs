using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace OctoprintDiscordBot.Core.ResponseModels;

public class Progress
{
	[JsonPropertyName("completion")]
	[NotNull]
	public float? Completion { get; set; }

	[JsonPropertyName("printTime")]
	[NotNull]
	public int? CurrentPrintTime { get; set; }

	[JsonPropertyName("printTimeLeft")]
	[NotNull]
	public int? EstimatedTimeLeft { get; set; }
}
