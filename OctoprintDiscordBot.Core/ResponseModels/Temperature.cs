﻿using Newtonsoft.Json;

namespace OctoprintDiscordBot.Core.ResponseModels;

public class Temperature
{
	[JsonProperty("tool0")]
	public Tool Tool0 { get; set; }

	[JsonProperty("bed")]
	public Bed Bed { get; set; }
}
