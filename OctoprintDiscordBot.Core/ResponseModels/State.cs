﻿using Newtonsoft.Json;

namespace OctoprintDiscordBot.Core.ResponseModels;

public class State
{
	[JsonProperty("text")]
	public string Text { get; set; }
}
