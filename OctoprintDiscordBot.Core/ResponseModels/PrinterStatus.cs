using Newtonsoft.Json;

namespace OctoprintDiscordBot.Core.ResponseModels;

public class PrinterStatus
{
	[JsonProperty("temperature")]
	public Temperature Temperature { get; set; }

	[JsonProperty("state")]
	public State State { get; set; }
}
