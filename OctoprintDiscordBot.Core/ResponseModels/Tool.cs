﻿using Newtonsoft.Json;

namespace OctoprintDiscordBot.Core.ResponseModels;

public class Tool
{
	[JsonProperty("actual")]
	public double Actual { get; set; }

	[JsonProperty("target")]
	public double Target { get; set; }
}
