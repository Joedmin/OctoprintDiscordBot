using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace OctoprintDiscordBot.Core.ResponseModels;

public class Job
{
	[JsonPropertyName("file")]
	[NotNull]
	public File? File { get; set; }
}

public class JobResponse
{
	[JsonPropertyName("job")]
	public Job Job { get; set; }

	[JsonPropertyName("progress")]
	public Progress Progress { get; set; }

	[JsonPropertyName("state")]
	public string State { get; set; }

	public Stream ImageStream { get; set; } = Stream.Null;

	[JsonPropertyName("error")]
	[NotNull]
	public string? Error { get; set; }
}
