using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace OctoprintDiscordBot.Core.Utils;

internal static class HttpClientExtensions
{
	/// <summary>
	/// Makes an HTTP POST request with the <paramref name="body"/> serialized as JSON with the body encoded in <see cref="Encoding.UTF8"/>.
	/// </summary>
	/// <typeparam name="T">Type of the <paramref name="body"/> param.</typeparam>
	/// <param name="httpClient">The <see cref="HttpClient"/> instance.</param>
	/// <param name="requestUri"><see cref="Uri"/> to make the request to.</param>
	/// <param name="body"><see langword="object"/> to serialize as JSON.</param>
	/// <param name="cancellationToken">Token to cancel the operation.</param>
	/// <returns>The task object representing the asynchronous operation.</returns>
	/// <remarks>
	///	Alternative to <see cref="HttpClient.PostAsync(string?, HttpContent?)"/> which uses the
	/// default encoding which causes the OctoPrint API to return <see cref="HttpStatusCode.BadRequest"/>.
	///	</remarks>
	public static Task<HttpResponseMessage> PostAsync<T>
		(this HttpClient httpClient, Uri requestUri, T body, CancellationToken cancellationToken = default)
	{
		var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
		return httpClient.PostAsync(requestUri, content, cancellationToken);
	}
}
