using Discord;
using Discord.Interactions;

namespace OctoprintDiscordBot.Core.Modules;

// Do not implement `IDisposable` to dispose the `OctoprintClient`!
// Modules are transients while the service is singleton!
public class OctoprintCommandBase : InteractionModuleBase<SocketInteractionContext>
{
	protected const string WarningMessage = "This may disrupt any ongoing print jobs!";
	protected readonly OctoprintClient octoprintClient;
	protected readonly EmbedBuilder embedBuilder = new EmbedBuilder()
		.WithTitle("OctoPrint")
		.WithColor(Constants.SuccessColor);

	public OctoprintCommandBase(OctoprintClient octoprintClient)
	{
		this.octoprintClient = octoprintClient;
	}

	protected Task RespondAsync()
		=> RespondAsync(embed: embedBuilder.Build());

	protected static MessageComponent CreateConfirmButton(string label, string customId) => new ComponentBuilder()
		.WithButton(label, customId, style: ButtonStyle.Danger)
		.Build();
}
