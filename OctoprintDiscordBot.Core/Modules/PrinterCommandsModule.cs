using Discord.Interactions;

namespace OctoprintDiscordBot.Core.Modules;

[EnabledInDm(false)]
public class PrinterCommandsModule : OctoprintCommandBase
{
	public PrinterCommandsModule(OctoprintClient octoprintClient)
		: base(octoprintClient)
	{
	}

	[SlashCommand("printer-status", "Gets the printer status.")]
	public async Task Status()
	{
		var printerStatusTesponse = await octoprintClient.GetPrinterStatusAsync().ConfigureAwait(false);
		embedBuilder
			.AddField("State", printerStatusTesponse.State.Text)
			.AddField("Hotend temperature",
$@"Target: {printerStatusTesponse.Temperature.Tool0.Target} °C
Actual: {printerStatusTesponse.Temperature.Tool0.Actual} °C")
			.AddField("Heatbed temperature",
$@"Target: {printerStatusTesponse.Temperature.Bed.Target} °C
Actual: {printerStatusTesponse.Temperature.Bed.Actual} °C");

		await RespondAsync().ConfigureAwait(false);
	}

	// TODO: Fix error with parameter names not mathing regex ^[-_\p{L}\p{N}\p{IsDevanagari}\p{IsThai}]{1,32}$
	//[SlashCommand("preheat", "Heats the printer to given temperatures.")]
	public async Task Preheat(
		[Summary("Nozzle temperatrue")][MinValue(0)][MaxValue(300)] int nozzle,
		[Summary("Heatbed temperatrue")][MinValue(0)][MaxValue(120)] int heatbed)
	{
		await octoprintClient
			.PrehatToTempsAsync(nozzle, heatbed)
			.ConfigureAwait(false);

		// TODO: Response message
	}
}
