using Discord.Interactions;

namespace OctoprintDiscordBot.Core.Modules;

[EnabledInDm(false)]
public class ServerCommandsModule : OctoprintCommandBase
{
	public ServerCommandsModule(OctoprintClient octoprintClient)
		: base(octoprintClient)
	{
	}

	// TODO: Write when server is back online
	// TODO: Add `no-btn` with deleting the original msg
	[SlashCommand("restart", "Restarts the OctoPrint server. " + WarningMessage)]
	public Task Restart()
		=> RespondAsync("Do you really wish to restart the OctoPrint server? " + WarningMessage,
			components: CreateConfirmButton("Yes", "restart-btn"));

	[ComponentInteraction("restart-btn")]
	public async Task RestartConfirmed()
	{
		await octoprintClient.RestartAsync().ConfigureAwait(false);

		embedBuilder.WithDescription("Restarting the OctoPrint server.");

		await RespondAsync().ConfigureAwait(false);
	}

	//TODO: Write when server is back online
	// TODO: Add `no-btn` with deleting the original msg
	[SlashCommand("restart-safe", "Restarts the OctoPrint server. " + WarningMessage)]
	public Task RestartSafe()
		=> RespondAsync("Do you really wish to restart the OctoPrint server in safe mode? " + WarningMessage,
			components: CreateConfirmButton("Yes", "restart-safe-btn"));

	[ComponentInteraction("restart-safe-btn")]
	public async Task RestartSafeConfirmed()
	{
		await octoprintClient.RestartAsync().ConfigureAwait(false);

		embedBuilder.WithDescription("Restarting the OctoPrint server in safe mode.");

		await RespondAsync().ConfigureAwait(false);
	}

	[SlashCommand("server-version", "Restarts the OctoPrint server. " + WarningMessage)]
	public async Task ServerVersion()
	{
		var info = await octoprintClient.GetServerVersionAsync().ConfigureAwait(false);
		embedBuilder
			.AddField("Api version", info.ApiVersion)
			.AddField("Server version:", info.ServerVersion)
			.AddField("OctoPrint version", info.OctoprintVersion);

		await RespondAsync().ConfigureAwait(false);
	}

	[SlashCommand("connect", "Connects to the configured OctoPrint server. This is done automatically on the bot startup.")]
	public async Task Connect()
	{
		var success = await octoprintClient.ConnectAsync(CancellationToken.None).ConfigureAwait(false);

		if (success)
		{
			embedBuilder.WithDescription("Successfully connected.");
		}
		else
		{
			embedBuilder.WithColor(Constants.ErrorColor).WithDescription("Host unrechable");
		}

		await RespondAsync().ConfigureAwait(false);
	}
}
