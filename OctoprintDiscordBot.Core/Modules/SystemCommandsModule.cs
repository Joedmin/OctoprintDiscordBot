using Discord.Interactions;

namespace OctoprintDiscordBot.Core.Modules;

[EnabledInDm(false)]
public class SystemCommandsModule : OctoprintCommandBase
{
	public SystemCommandsModule(OctoprintClient octoprintClient)
		: base(octoprintClient)
	{
	}

	// TODO: Add `no-btn` with deleting the original msg
	[SlashCommand("shutdown", "Shutdowns the system. " + WarningMessage)]
	public Task Shutdown()
		=> RespondAsync("Do you really wish to shutdown? " + WarningMessage,
			components: CreateConfirmButton("Yes", "shutdown-btn"));

	[ComponentInteraction("shutdown-btn")]
	public async Task ShutdownConfirmed()
	{
		await octoprintClient.ShutdownAsync().ConfigureAwait(false);

		embedBuilder.WithDescription("Shutting down the system.");

		await RespondAsync().ConfigureAwait(false);
	}

	// TODO: Add `no-btn` with deleting the original msg
	[SlashCommand("reboot", "Reboots the system. " + WarningMessage)]
	public Task Reboot()
		=> RespondAsync("Do you really wish to reboot? " + WarningMessage,
			components: CreateConfirmButton("Yes", "reboot-btn"));

	[ComponentInteraction("reboot-btn")]
	public async Task RebootConfirmed()
	{
		await octoprintClient.RebootAsync().ConfigureAwait(false);

		embedBuilder.WithDescription("Rebooting the system.");

		await RespondAsync().ConfigureAwait(false);
	}
}
