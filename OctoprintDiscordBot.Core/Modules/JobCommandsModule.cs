using Discord.Interactions;

namespace OctoprintDiscordBot.Core.Modules;

[EnabledInDm(false)]
public class JobCommandsModule : OctoprintCommandBase
{
	public JobCommandsModule(OctoprintClient octoprintClient)
		: base(octoprintClient)
	{
	}

	[SlashCommand("status", "Gets the printer status.")]
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Minor Code Smell", "S1075:URIs should not be hardcoded", Justification = "Not an issue.")]
	public async Task CurrentJobStatus()
	{
		var jobResponse = await octoprintClient.GetCurrentJobStatusAsync().ConfigureAwait(false);

		if (jobResponse.State == "Offline after error")
		{
			embedBuilder
				.WithColor(Constants.ErrorColor)
				.WithTitle("An exception occurred")
				.WithDescription(jobResponse.Error);

			await RespondAsync();
			return;
		}

		embedBuilder
			.AddField("State", jobResponse.State, true)
			.WithImageUrl("attachment://screenshot.jpeg");

		// No job has been selected when `null`.
		if (jobResponse.Job.File.Name is not null)
		{
			embedBuilder
				.AddField("Progress", (int)jobResponse.Progress.Completion + " %", true)
				.AddField("File", jobResponse.Job.File.Name)
				.AddField("Size", Math.Round(jobResponse.Job.File.Size.Value / 1024M, 1) + " Kb") // TODO: Show the largest possible unit
				.AddField("Current print time", TimeSpan.FromSeconds(jobResponse.Progress.CurrentPrintTime.Value).ToString(), true)
				.AddField("Estimated time left", TimeSpan.FromSeconds(jobResponse.Progress.EstimatedTimeLeft.Value).ToString(), true);
		}
		else
		{
			embedBuilder.WithDescription("No job has been selected.");
		}

		if (jobResponse.State != "Printing")
		{
			embedBuilder.WithColor(Constants.InformationColor);
		}

		await RespondWithFileAsync(jobResponse.ImageStream, "screenshot.jpeg", embed: embedBuilder.Build(), ephemeral: true).ConfigureAwait(false);
	}
}
