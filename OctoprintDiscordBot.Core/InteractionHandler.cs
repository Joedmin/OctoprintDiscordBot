using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace OctoprintDiscordBot.Core;

public class InteractionHandler : IDisposable
{
	private readonly DiscordClient discordClient;
	private readonly InteractionService handler;
	private readonly IServiceProvider services;
	private readonly ILogger<InteractionHandler> logger;
	private bool disposed;

	public InteractionHandler(DiscordClient discordClient, InteractionService handler, IServiceProvider services, ILogger<InteractionHandler> logger)
	{
		this.discordClient = discordClient;
		this.handler = handler;
		this.services = services;
		this.logger = logger;
	}

	public async Task InitializeAsync()
	{
		discordClient.Ready += ReadyAsync;
		discordClient.InteractionCreated += HandleInteraction;

		await handler.AddModulesAsync(Assembly.GetAssembly(typeof(DiscordClient)), services).ConfigureAwait(false);
		await discordClient.ConnectAsync().ConfigureAwait(false);
	}

	private async Task ReadyAsync()
	{
		try
		{
			await handler.RegisterCommandsGloballyAsync(true).ConfigureAwait(false);
			logger.LogInformation("Discord commands registered successfully.");
		}
		catch (Exception ex)
		{
			logger.LogError("Could not register commands due to an error: {error}", ex);
			throw;
		}
	}

	private async Task HandleInteraction(SocketInteraction interaction)
	{
		try
		{
			var context = new SocketInteractionContext(discordClient, interaction);
			var result = await handler.ExecuteCommandAsync(context, services).ConfigureAwait(false);

			if (result.IsSuccess)
			{
				return;
			}

			var exception = ((ExecuteResult)result).Exception;

			var embedBuilder = new EmbedBuilder().WithColor(Constants.ErrorColor);

			if (exception is TaskCanceledException)
			{
				logger.LogError("Request to OctoPrint timed out.");
				embedBuilder
					.WithTitle("Request timed out.")
					.WithDescription("Make sure the OctoPrint is running.");
			}
			else
			{
				logger.LogError("Command failed with exception: {error}", ((ExecuteResult)result).Exception);
				embedBuilder
					.WithTitle("An exception occurred")
					.WithDescription(exception.Message + "\nCheck logs for stack trace.");
			}

			await interaction.RespondAsync(embed: embedBuilder.Build(), ephemeral: true).ConfigureAwait(false);
		}
		catch (Exception exception)
		{
			logger.LogError("Interaction failed with exception: {error}", exception);

			// TODO: Handle
			throw;
		}
	}

	#region Dispose

	protected virtual void Dispose(bool disposing)
	{
		if (!disposing && !disposed)
		{
			discordClient?.DisconnectAsync();
			discordClient?.Dispose();
			handler?.Dispose();
			disposed = true;
		}
	}

	~InteractionHandler()
	{
		Dispose(false);
	}

	public void Dispose()
	{
		Dispose(true);
		GC.SuppressFinalize(this);
	}

	#endregion
}
