using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;

namespace OctoprintDiscordBot.Core;

// TODO: Do not reconnect when server responds with 401
// TODO: Create guild and assign all commands to it
// TODO: Send messages to its own channel
public class DiscordClient : DiscordSocketClient
{
	private readonly ILogger<DiscordClient> logger;
	private readonly string token;

	public DiscordClient(string token, ILogger<DiscordClient> logger)
	{
		this.logger = logger;
		this.token = token;
		Connected += OnConnected;
		Disconnected += OnDisconnected;
	}

	public async Task ConnectAsync()
	{
		await LoginAsync(TokenType.Bot, token).ConfigureAwait(false);
		await StartAsync().ConfigureAwait(false);
	}

	public Task DisconnectAsync() => LogoutAsync();

	private Task OnConnected()
	{
		logger.LogInformation("Sucessfully connected to Discord!");
		return Task.CompletedTask;
	}

	private Task OnDisconnected(Exception exception)
	{
		if (exception is not TaskCanceledException)
		{
			// Application is not requested to shut down.
			logger.LogError("Disconnected from discord. Reason: {reason}.", exception.Message);
		}

		return Task.CompletedTask;
	}
}
