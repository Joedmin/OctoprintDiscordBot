using Discord;

namespace OctoprintDiscordBot.Core;

public static class Constants
{
	public static readonly Color SuccessColor = Color.Green;
	public static readonly Color InformationColor = Color.Blue;
	public static readonly Color WarningColor = Color.Orange;
	public static readonly Color ErrorColor = Color.DarkRed;
}
