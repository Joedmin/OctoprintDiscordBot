using Microsoft.Extensions.Logging;
using OctoprintDiscordBot.Core.ResponseModels;
using OctoprintDiscordBot.Core.Utils;
using System.Net;
using System.Net.Http.Json;

namespace OctoprintDiscordBot.Core;

// TODO: Check if octoprint server is online (periodically?)
// TODO: Add `CheckConnection`
public class OctoprintClient : IDisposable
{
	private readonly HttpClient httpClient;
	private readonly Uri baseOctoprintUri;
	private readonly ILogger<OctoprintClient> logger;
	private bool hasCamera = false;
	private bool disposed;

	public OctoprintClient(Uri baseOctoprintUri, HttpClient httpClient, ILogger<OctoprintClient> logger)
	{
		this.baseOctoprintUri = baseOctoprintUri;
		this.httpClient = httpClient;
		this.logger = logger;
	}

	// TODO: Fail if request timeouts
	// TODO: Retry connection few times when the server is not reachable?
	public async ValueTask<bool> ConnectAsync(CancellationToken cancellationToken)
	{
		try
		{
			var response = await httpClient.PostAsync(
				GetApiEndpoint("login"),
				new { passive = true },
				cancellationToken).ConfigureAwait(false);

			if (!response.IsSuccessStatusCode)
			{
				return false;
			}

			await DetectCameraAsync();

			logger.LogInformation("Sucessfully connected to OctoPrint!");
			return true;
		}
		catch (HttpRequestException exception)
		{
			logger.LogError("Could not connect to OctoPrint. Reason: {reason}.", exception);
		}
		catch (TaskCanceledException)
		{
			logger.LogError("OctoPrint server is unreachable.");
		}

		return false;
	}

	private async Task DetectCameraAsync()
	{
		try
		{
			// TODO: Handle non-default octoprint endpoint
			await httpClient.GetAsync(new Uri(baseOctoprintUri, "webcam/?action=snapshot")).ConfigureAwait(false);
			hasCamera = true;
			return;
		}
		catch (TaskCanceledException)
		{
			// Ignore
		}
		catch (HttpRequestException ex)
			when (ex.StatusCode == HttpStatusCode.ServiceUnavailable)
		{
			// 503 in this case means that no camera is attached thus we can ignore this exception.
		}

		hasCamera = false;
	}

	// TODO: Add missing methods for discord commands

	// If the API returns `null` then it must mean that it is not OctoPrint API
#pragma warning disable CS8619 // Nullability of reference types in value doesn't match target type.

	/// <summary>
	/// Gets the server informations.
	/// </summary>
	/// <returns>The task object representing the asynchronous operation.</returns>
	public Task<ServerVersionInfo> GetServerVersionAsync()
		=> httpClient.GetFromJsonAsync<ServerVersionInfo>(GetApiEndpoint("version"));

	/// <summary>
	/// Shuts down the system running OctoPrint.
	/// </summary>
	/// <returns>The task object representing the asynchronous operation.</returns>
	public async Task ShutdownAsync()
	{
		var response = await httpClient
			.PostAsync(GetApiEndpoint("/system/commands/core/shutdown"), null)
			.ConfigureAwait(false);
		response.EnsureSuccessStatusCode();
	}

	/// <summary>
	/// Reboots the system running OctoPrint.
	/// </summary>
	/// <returns>The task object representing the asynchronous operation.</returns>
	public async Task RebootAsync()
	{
		var response = await httpClient
			.PostAsync(GetApiEndpoint("/system/commands/core/reboot"), null)
			.ConfigureAwait(false);
		response.EnsureSuccessStatusCode();
	}

	/// <summary>
	/// Restarts the OctoPrint server.
	/// </summary>
	/// <returns>The task object representing the asynchronous operation.</returns>
	public async Task RestartAsync()
	{
		var response = await httpClient
			.PostAsync(GetApiEndpoint("/system/commands/core/restart"), null)
			.ConfigureAwait(false);
		response.EnsureSuccessStatusCode();
	}

	/// <summary>
	/// Restarts the OctoPrint server in safe mode.
	/// </summary>
	/// <returns>The task object representing the asynchronous operation.</returns>
	public async Task RestartSafeAsync()
	{
		var response = await httpClient
			.PostAsync(GetApiEndpoint("/system/commands/core/restart_safe"), null)
			.ConfigureAwait(false);
		response.EnsureSuccessStatusCode();
	}

	// TODO: Fix `System.Text.Json.JsonException` when the print has not been selected yet
	// TODO: Handle unconected camera
	/// <summary>
	/// Gets the informations about the current job.
	/// </summary>
	/// <returns>The task object representing the asynchronous operation.</returns>
	public async Task<JobResponse> GetCurrentJobStatusAsync()
	{
		var response = (await httpClient.GetFromJsonAsync<JobResponse>(GetApiEndpoint("job")).ConfigureAwait(false))!;

		if (hasCamera)
		{
			response.ImageStream = await httpClient.GetStreamAsync(new Uri(baseOctoprintUri, "webcam/?action=snapshot")).ConfigureAwait(false);
		}

		return response;
	}

	/// <summary>
	/// Preheats the printer to the given temperatures.
	/// </summary>
	/// <param name="nozzleTemp">Target tempreature of the nozzle.</param>
	/// <param name="bedTemp">Target temperature of the heatbed.</param>
	/// <returns>The task object representing the asynchronous operation.</returns>
	public async Task PrehatToTempsAsync(int nozzleTemp, int bedTemp)
	{
		var nozzleResponse = await httpClient
			.PostAsync(GetApiEndpoint("/printer/tool"),
				new
				{
					command = "target",
					targets = new
					{
						tool0 = nozzleTemp,
					},
				})
			.ConfigureAwait(false);
		nozzleResponse.EnsureSuccessStatusCode();

		var bedResponse = await httpClient
			.PostAsync(GetApiEndpoint("/printer/bed"),
				new
				{
					command = "target",
					target = bedTemp,
				})
			.ConfigureAwait(false);
		bedResponse.EnsureSuccessStatusCode();
	}

	public Task<PrinterStatus> GetPrinterStatusAsync()
		=> httpClient.GetFromJsonAsync<PrinterStatus>(GetApiEndpoint("printer"));

#pragma warning restore CS8619 // Nullability of reference types in value doesn't match target type.

	private Uri GetApiEndpoint(string? apiEndpoint = null) => new(baseOctoprintUri, "api/" + apiEndpoint);

	#region Dispose

	protected virtual void Dispose(bool disposing)
	{
		if (disposing && !disposed)
		{
			httpClient?.CancelPendingRequests();
			httpClient?.Dispose();
			disposed = true;
		}
	}

	~OctoprintClient() => Dispose(false);

	public void Dispose()
	{
		Dispose(true);
		GC.SuppressFinalize(this);
	}

	#endregion
}
