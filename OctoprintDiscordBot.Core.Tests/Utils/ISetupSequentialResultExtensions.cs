using Moq;
using Moq.Language;
using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace OctoprintDiscordBot.Core.Tests.Utils;

internal static class ISetupSequentialResultExtensions
{
	public static ISetupSequentialResult<Task<HttpResponseMessage>> ReturnsHttpResponseAsync<T>(
		this ISetupSequentialResult<Task<HttpResponseMessage>> setupSequential,
		HttpStatusCode statusCode = HttpStatusCode.OK,
		T? responseBody = default)
	{
		return setupSequential.ReturnsAsync(new HttpResponseMessage()
		{
			StatusCode = statusCode,
			Content = responseBody is null
				? null
				: new StringContent(JsonConvert.SerializeObject(responseBody), Encoding.UTF8, "application/json"),
		});
	}
}
