using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Moq.Language;
using Moq.Protected;
using OctoprintDiscordBot.Core.ResponseModels;
using OctoprintDiscordBot.Core.Tests.Utils;
using System.Net;
using System.Text;

namespace OctoprintDiscordBot.Core.Tests;

public class OctoprintClientTests
{
	private readonly Uri apiUri = new("http://localhost");
	private readonly OctoprintClient sut;
	private readonly HttpClient httpClient;
	private readonly Mock<HttpMessageHandler> messageHandler;

	public OctoprintClientTests()
	{
		messageHandler = new(MockBehavior.Strict);
		httpClient = new HttpClient(messageHandler.Object)
		{
			BaseAddress = apiUri,
		};
		sut = new(apiUri, httpClient, NullLogger<OctoprintClient>.Instance);
	}

	[Fact]
	public async Task ConnectConnectionErrorTest()
	{
		CreateResponseSetup()
			.ThrowsAsync(new HttpRequestException());

		(await sut.ConnectAsync(CancellationToken.None).ConfigureAwait(false)).Should().BeFalse();

		VerifyMessageHandlerCalls(1);
	}

	[Fact]
	public async Task ConnectInvalidTokenTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync(HttpStatusCode.Forbidden, new { passive = "" });

		(await sut.ConnectAsync(CancellationToken.None).ConfigureAwait(false)).Should().BeFalse();

		VerifyMessageHandlerCalls(1);
	}

	[Fact]
	public async Task ConnectValidTokenCameraAvailableTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync(HttpStatusCode.OK, new { passive = "" })
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.OK);

		(await sut.ConnectAsync(CancellationToken.None).ConfigureAwait(false)).Should().BeTrue();

		VerifyMessageHandlerCalls(2);
	}

	[Fact]
	public async Task ConnectValidTokenCameraUnavailableTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync(HttpStatusCode.OK, new { passive = "" })
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.ServiceUnavailable);

		(await sut.ConnectAsync(CancellationToken.None).ConfigureAwait(false)).Should().BeTrue();

		VerifyMessageHandlerCalls(2);
	}

	[Fact]
	public async Task ConnectValidTokenCameraTimeoutTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync(HttpStatusCode.OK, new { passive = "" })
			.Throws(new TaskCanceledException());

		(await sut.ConnectAsync(CancellationToken.None).ConfigureAwait(false)).Should().BeTrue();

		VerifyMessageHandlerCalls(2);
	}

	[Fact]
	public async Task GetServerInfoTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync(HttpStatusCode.OK, new
			{
				api = "0.1",
				server = "1.3.10",
				text = "OctoPrint 1.3.10",
			});

		(await sut.GetServerVersionAsync().ConfigureAwait(false))
			.Should().BeEquivalentTo(new ServerVersionInfo
			{
				ApiVersion = "0.1",
				ServerVersion = "1.3.10",
				OctoprintVersion = "OctoPrint 1.3.10",
			});

		VerifyMessageHandlerCalls(1);
	}

	[Fact]
	public void ShutdownTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.NoContent);

		new Action(() => sut.ShutdownAsync().ConfigureAwait(false)).Should().NotThrow();

		VerifyMessageHandlerCalls(1);
	}

	[Fact]
	public void RebootTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.NoContent);

		new Action(() => sut.RebootAsync().ConfigureAwait(false)).Should().NotThrow();

		VerifyMessageHandlerCalls(1);
	}

	[Fact]
	public void RestartTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.NoContent);

		new Action(() => sut.RestartAsync().ConfigureAwait(false)).Should().NotThrow();

		VerifyMessageHandlerCalls(1);
	}

	[Fact]
	public void RestartSafeTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.NoContent);

		new Action(() => sut.RestartSafeAsync().ConfigureAwait(false)).Should().NotThrow();

		VerifyMessageHandlerCalls(1);
	}

	[Fact]
	public async Task GetCurrentJobStatusCameraNotConected()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync(responseBody: SampleJobResponseAnonymous());

		(await sut.GetCurrentJobStatusAsync().ConfigureAwait(false))
			.Should().BeEquivalentTo(SampleJobResponse());

		VerifyMessageHandlerCalls(1);
	}

	[Fact]
	public async Task GetCurrentJobStatusCameraConected()
	{
		var contentMock = new Mock<HttpContent>();

		var imageStream = new MemoryStream();
		imageStream.Write(Encoding.UTF8.GetBytes("Hello world"));

		var response = SampleJobResponse();
		response.ImageStream = imageStream;

		CreateResponseSetup()
			.ReturnsHttpResponseAsync(HttpStatusCode.OK, new { passive = "" })
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.OK)

			.ReturnsHttpResponseAsync(responseBody: SampleJobResponseAnonymous())
			.ReturnsAsync(new HttpResponseMessage() { Content = contentMock.Object });

		contentMock.Protected()
			.Setup<Task<Stream>>("CreateContentReadStreamAsync", It.IsAny<CancellationToken>())
			.ReturnsAsync(imageStream);

		// Connect manually here so the camera is detected correctly
		(await sut.ConnectAsync(CancellationToken.None).ConfigureAwait(false)).Should().BeTrue();
		(await sut.GetCurrentJobStatusAsync().ConfigureAwait(false)).Should().BeEquivalentTo(response);

		VerifyMessageHandlerCalls(4);
	}

	[Fact]
	public void PrehatToTempsSuccessfullStatusCodesTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.OK)
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.OK);

		new Action(() => sut.PrehatToTempsAsync(200, 50).ConfigureAwait(false)).Should().NotThrow();

		VerifyMessageHandlerCalls(2);
	}

	[Fact]
	public async Task PrehatToTempsUnuccessfullFirstStatusCodeTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.BadRequest);

		await new Func<Task>(async () => await sut.PrehatToTempsAsync(200, 50).ConfigureAwait(false))
			.Should().ThrowAsync<HttpRequestException>();

		VerifyMessageHandlerCalls(1);
	}

	[Fact]
	public async Task PrehatToTempsUnuccessfullSecondStatusCodeTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.OK)
			.ReturnsHttpResponseAsync<object>(HttpStatusCode.BadRequest);

		await new Func<Task>(async () => await sut.PrehatToTempsAsync(200, 50).ConfigureAwait(false))
			.Should().ThrowAsync<HttpRequestException>();

		VerifyMessageHandlerCalls(2);
	}

	[Fact]
	public async Task GetPrinterStatusTest()
	{
		CreateResponseSetup()
			.ReturnsHttpResponseAsync(responseBody: new
			{
				temperature = new
				{
					tool0 = new
					{
						actual = 214.8821,
						target = 220,
					},
					bed = new
					{
						actual = 50.221,
						target = 70,
					},
					history = new object[]
					{
						new
						{
							tool0 = new
							{
								actual = 214.8821,
								target = 220,
							},
							bed = new
							{
								actual = 50.221,
								target = 70,
							},
						},
						new
						{
							tool0 = new
							{
								actual = 212.32,
								target = 220,
							},
							bed = new
							{
								actual = 49.1123,
								target = 70,
							},
						},
					},
				},
				state = new
				{
					text = "Operational",
				},
			});


		(await sut.GetPrinterStatusAsync().ConfigureAwait(false))
			.Should().BeEquivalentTo(new PrinterStatus
			{
				Temperature = new Temperature
				{
					Tool0 = new Tool
					{
						Actual = 214.8821,
						Target = 220,
					},
					Bed = new Bed
					{
						Actual = 50.221,
						Target = 70,
					},
				},
				State = new State
				{
					Text = "Operational",
				},
			});

		VerifyMessageHandlerCalls(1);
	}

	private ISetupSequentialResult<Task<HttpResponseMessage>> CreateResponseSetup()
	{
		return messageHandler
			.Protected()
			.SetupSequence<Task<HttpResponseMessage>>(
				"SendAsync",
				ItExpr.IsAny<HttpRequestMessage>(),
				ItExpr.IsAny<CancellationToken>());
	}

	private void VerifyMessageHandlerCalls(int countAmount)
	{
		messageHandler
			.Protected()
			.Verify<Task<HttpResponseMessage>>(
				"SendAsync",
				Times.Exactly(countAmount),
				ItExpr.IsAny<HttpRequestMessage>(),
				ItExpr.IsAny<CancellationToken>());
	}

	private static JobResponse SampleJobResponse()
		=> new()
		{
			Job = new()
			{
				File = new()
				{
					Name = "whistle_v2.gcode",
					Size = 1468987,
					UploadDate = 1378847754,
				},
			},
			Progress = new()
			{
				Completion = 0.2298468264184775F,
				CurrentPrintTime = 276,
				EstimatedTimeLeft = 912,
			},
			State = "Printing"
		};

	private static object SampleJobResponseAnonymous()
		=> new
		{
			job = new
			{
				file = new
				{
					name = "whistle_v2.gcode",
					size = 1468987,
					date = 1378847754,
				},
				estimatedPrintTime = 8811,
				filament = new
				{
					tool0 = new
					{
						length = 810,
						volume = 5.36,
					},
				},
			},
			progress = new
			{
				completion = 0.2298468264184775,
				printTime = 276,
				printTimeLeft = 912,
			},
			state = "Printing",
		};
}
